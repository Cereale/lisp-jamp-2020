(global window-width 1080)
(global window-height 720)
(global level-length 1000)
(global max-dice-value 6)
(global level-set [[2 2 2 2 2 2 1 1 1 2 2 2 2 1 1 1 1 1 2 2 2 2 2 1 1]
                   [2 2 1 2 1 2 1 2 2 2 2 2 1 2 1 1 1 1 1 2 1 2 1 1 1]
                   [2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 1 2 2 2]
                   [2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2]
                   [2 2 2 1 1 1 2 2 2 1 1 1 2 2 2 1 1 1 2 2 2 1 1 1 2]
                   [2 2 1 2 2 1 2 2 1 2 2 1 2 2 1 2 2 1 2 2 1 2 2 1 2]
                   [2 2 1 1 2 2 1 1 2 2 1 1 2 2 1 1 2 2 1 1 2 2 1 1 2]
                   [2 1 2 1 1 2 1 1 1 2 1 1 1 1 2 1 1 1 1 1 2 2 2 2 2]
                   [2 2 1 2 2 1 2 2 2 1 2 2 2 2 1 2 2 2 2 2 1 2 2 2 2]
                   [2 1 2 1 1 2 2 2 1 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2]
                   [2 1 2 1 1 2 2 2 1 1 1 1 1 1 2 2 2 1 1 1 1 1 2 2 2]
                   ])
(fn add [x y]
  (+ x y)
  )

(fn dice [max-value]
  (love.math.random 0 max-value)
  )

(fn max [table]
  (var max-value -10000000000000000000000000)
  (for [i 1 (length table)]
    (if (< max-value (. table i))
        (set max-value (. table i))
        )
    )
  max-value
  )


(fn min [table]
  (var min-value 10000000000000000000000000)
  (for [i 1 (length table)]
    (if (> min-value (. table i))
        (set min-value (. table i))
        )
    )
  min-value
  )


(fn mod1 [value step]   ; modulo function starting at 1
  (+ 1 (% (- value 1) step))
  )


(fn reduce [tbl fct]
  (var counter (. tbl 1))
  (for [i 2 (length tbl)]
    (set counter (fct counter (. tbl i)))
    )
  counter
)


(fn count-duplicates [dices value dices-active]
  (var counter 0)
  (for [i 1 (length dices)]
    (if (= (. dices i) value)
        (if (= (. dices-active i) 1)
            (set counter (+ counter 1))
            )
        )
    )
  counter
  )


(fn get-combo [dices dices-active]
  (var combo [0 0 0 0 0])  ; [pair, triple, full, quadruple, 5 of a kind]
  (var duplicates {})
  (for [i 1 max-dice-value]
    (tset duplicates i (count-duplicates dices i dices-active))
    )
  (table.sort duplicates)
  (let [max (. duplicates (length duplicates))
        second-max (. duplicates (- (length duplicates) 1))]
    (if (= max 5)
        (set combo [1 1 0 1 1])
        (= max 4)
        (set combo [1 1 0 1 0])
        (= max 2)
        (set combo [1 0 0 0 0])
        )
    (when (= max 3)
      (if (= second-max 2)
          (set combo [1 1 1 0 0])
          (set combo [1 1 0 0 0])
          )
      )
    )
  combo
  )


(fn reset-round []
  (global game-state 1) ; 1 = waiting for player input / 2 = win / 0 = waiting for confirmation
  (global dices [0 0 0 0 0 0])
  (global locked-dices [0 0 0 0 0 0])
  (global roll-counter 3)
  (global dices-active [1 1 1 1 1 0])
  (global combo (get-combo dices dices-active))
  (global combo-used false)
  (global selector 0)   ; 0 = inactive selector
  )


(fn build-level []
  (global level [])  ; 1 = spikes, 2 = free space
  (let [len (length (. level-set 1))]
    (for [i 1 level-length len]
      (let [level-brick (. level-set (love.math.random 1 (length level-set)))]
        (for [j i (+ i len)]
          (tset level j (. level-brick (mod1 j len)))
          )
        )
      )
    )
  )


(fn love.load []  
  (love.window.setMode window-width window-height)
  (love.graphics.setBackgroundColor 0 0.075 0.15)
  (love.keyboard.setKeyRepeat true)
  (global player 1)
  (build-level)
  (reset-round)
  (global combo-used true)
  (global camera-offset 0)
  (global sum 0)
  (global checkpoint 1)
  (global moves 0)
  (global helper-text "")
)


(fn roll-dice [index]
  (tset dices index (dice 6))
  )

(fn handle-combos []
  (when (= game-state 11)
    (global selector 1)
    (global game-state 21)
    )
  (when (= game-state 12)
    (global selector 1)
    (global game-state 22)
    )
  (when (= game-state 13)
    (tset dices 6 (dice 6))
    (tset dices-active 6 1)
    (global game-state 1)
    )
  (when (= game-state 14)
      (global roll-counter (+ roll-counter 1))
      (global game-state 1)
      )
  (when (= game-state 15)
    (global selector 1)
    (global game-state 25)
    (global dices-copy [])
    (for [i 1 6]
      (tset dices-copy i (. dices i)))
    )
  (global combo-used true)
  )



(fn dice-sum []
  (var list [])
  (for [i 1 (length dices)]
    (if (= 1 (. dices-active i))
        (if (~= (. dices i) nil)
            (tset list i (. dices i))))
    )
  (for [i (length list) 1 -1]
    (if (= nil (. list i))
        (table.remove list i)
        )
    )
  (var out (reduce list add))
  out
  )


(fn set-helper-text []
  (if (= game-state 1)
      (if (> roll-counter 0)
          (global helper-text "Roll the dices (Space), use an ability (QWERT) or confirm roll (Enter)")
          (global helper-text "Use an ability (QWERT) or confirm roll (Enter)")
          )
      (= game-state 21)
      (global helper-text "2 of a kind : Select (arrows) dice to reroll and confirm (Enter)")
      (= game-state 22)
      (global helper-text "3 of a kind : Select (arrows) dice to remove and confirm (Enter)")
      (= game-state 25)
      (global helper-text "Yahtzee : Select (arrows) dice and change its value (arrows)")
       )
  )

(fn love.update [dt]
  (when (= game-state 1)
    (if (= (. level player) 1)
        (global player checkpoint)
        (global camera-offset (max [0 (- player 10)]))
        )
    (when (> player level-length)
      (global game-state 2)
      (global player (+ 2 level-length))
      (print "Victory")
      )
    )
  (if (> game-state 10)
      (handle-combos))
  (global sum (dice-sum))
  (global checkpoint (+ 1 (* 100 (math.floor (/ player 100)))))
  (set-helper-text)
  )


(fn love.keypressed [key scancode isrepeat]
  (when (= key "space")
    (when (= game-state 1)
      (when (> roll-counter 0)
        (for [i 1 6]
          (if (= (. locked-dices i) 0)
              (if (= (. dices-active i) 1)
                  (tset dices i (dice max-dice-value)))
              )
          )
        (global roll-counter (- roll-counter 1))
        (global combo (get-combo dices dices-active))
        )
    )
  )
  (when (= key "return")
    (when (= game-state 1)
      (global player (+ player (dice-sum)))
      (reset-round)
      (global camera-offset (max [0 (- player 30)]))
      (global moves (+ moves 1))
      )
    
    (when (= game-state 21)
      (roll-dice selector)  ; rerolling selected dice
      (global game-state 1)
      (global selector 0)
      )
    (when (= game-state 22)
      (tset dices-active selector 0)   ; removing selected dice
      (tset locked-dices selector 0)
      (global game-state 1)
      (global selector 0)
      )
    (when (= game-state 25)
      (let [temp (. dices selector)]
           (for [i 1 6]
             (tset dices i (. dices-copy i))
             )
           (tset dices selector temp)
           )
      (global game-state 1)
      (global selector 0)
      )
    )

  
  (if (= key "1")
      (tset locked-dices 1 (- 1 (. locked-dices 1)))
      (= key "2")
      (tset locked-dices 2 (- 1 (. locked-dices 2)))
      (= key "3")
      (tset locked-dices 3 (- 1 (. locked-dices 3)))
      (= key "4")
      (tset locked-dices 4 (- 1 (. locked-dices 4)))
      (= key "5")
      (tset locked-dices 5 (- 1 (. locked-dices 5)))
      (= key "6")
      (tset locked-dices 6 (- 1 (. locked-dices 6)))
      )


  (when (= selector 0)
    (if (= key "right")
        (global camera-offset (+ camera-offset 1))
        (= key "left")
        (global camera-offset (- camera-offset 1))
        (= key "down")
        (global camera-offset (max [0 (- player 30)]))
        )
    )
  (when (> selector 0)
    (when (= game-state 25)
      (when (= key "up")
        (tset dices selector (mod1 (+ (. dices selector) 1) 6)))
      (when (= key "down")
        (tset dices selector (mod1 (- (. dices selector) 1) 6)))
      )
    (when (= key "right")
      (if (= game-state 25)
          (tset dices selector (. dices-copy selector)))
      (global selector (mod1 (+ selector 1) 6))
      (while (= 0 (. dices-active selector))
        ;(print "dice not active")
        (global selector (mod1 (+ selector 1) 6))
        )
      )
    (when  (= key "left")
      (if (= game-state 25)
          (tset dices selector (. dices-copy selector)))
      (global selector (mod1 (- selector 1) 6))
      (while (= 0 (. dices-active selector) 6)
        (global selector (mod1 (- selector 1) 6))
        )
      )
    )

  (if (= combo-used false)
      (if (= key "q")
          (global game-state 11)  ; bonus 1 (pair)
          (= key "w")
          (global game-state 12)  ; bonus 2 (3 of a kind)
          (= key "e")
          (global game-state 13)  ; bonus 3 (full)
          (= key "r")
          (global game-state 14)  ; bonus 4 (4 of a kind)
          (= key "t")
          (global game-state 15)  ; bonus 5 (yahtzee)
          )
      )
  )


(fn draw-dices []
  (let [x-min 200
        y 600
        w 50]
    (for [i 1 5]
      (let [x (+ x-min (* 2 w i))]
        (when (= (. dices-active i) 1)
          (if (= (. locked-dices i) 0)
              (love.graphics.setColor 1 1 1)
              (love.graphics.setColor 0.7 0.5 0.5)
              )
          (love.graphics.rectangle "line" x y w w)
          (love.graphics.print (tostring (. dices i)) x y 0 2 2 (/ w -6) (/ w -9))
          (love.graphics.print (tostring i) x y 0 1 1 -2 -1)
          )
        (when (= selector i)
          (love.graphics.setColor 1 1 0.7)
          (love.graphics.rectangle "line" (- x 2) (- y 2) (+ w 4) (+ w 4))
          )
        )
      )
    (when (= (. dices-active 6) 1)
      (if (= (. locked-dices 6) 0)
          (love.graphics.setColor 1 1 1)
          (love.graphics.setColor 0.7 0.5 0.5)
          )
      (love.graphics.rectangle "line" (+ x-min (* 2 w 6)) y w w)
      (love.graphics.print (tostring (. dices 6)) (+ x-min (* 2 w 6)) y 0 2 2 (/ w -6) (/ w -6))
      )
    (love.graphics.setColor 1 1 1)
    (love.graphics.print (tostring sum) 535 215 0 2 2)
    )
  )


(fn draw-special []
  (let [x-min -100
        y 75
        h 50
        w 130
        combo-names ["2 of a kind" "3 of a kind" "Full" "4 of a kind" "Yahtzee"]
        keys ["Q" "W" "E" "R" "T"]]
    (for [i 1 5]
      (if (= (. combo i) 0)
          (love.graphics.setColor 1 1 1)
          (if (= combo-used false)
              (love.graphics.setColor 0.5 0.9 0.5)
              )
          )
      (let [x (+ x-min (* 1.5 w i))]
        (love.graphics.rectangle "line" x y w h)
        (love.graphics.print (. combo-names i) x y 0 1.5 1.5 (/ w -10) (/ h -6))
        (love.graphics.print (tostring (. keys i)) x y 0 0.9 0.9 -2 -1)
        )
      )
    )
  (love.graphics.setColor 1 1 1)
  )



(fn draw-level []
  (let [y 400
        tile-width 16
        x-min (- 100 (* camera-offset tile-width))
        drawn-player player]
    (love.graphics.setColor 1 1 1)
    (love.graphics.line (+ x-min tile-width) (+ y (/ tile-width 2)) (+ x-min (* level-length tile-width) tile-width) (+ y (/ tile-width 2)))
    (for [i 1 (+ 1 level-length)]
      (let [x (+ x-min (* i tile-width))]
        (when (= (. level i) 1)
            (love.graphics.setColor 0.5 0.4 0.8)
            (love.graphics.rectangle "fill" x (- y 8) tile-width tile-width)
            )
        (when (= 1 (mod1 i 100))
          (if (>= checkpoint i)
              (love.graphics.setColor 0.5 0.9 0.5)
              (love.graphics.setColor 0.9 0.9 0.5)
              )
          (love.graphics.line x (+ y 4) x (- y 32))
          (love.graphics.polygon "fill" x (- y 24) x (- y 32) (+ x 8) (- y 28))
          (if (= i 1)
              (love.graphics.print "Start" (- x 12) (+ y 12))
              (= i 1001)
              (love.graphics.print "Finish" (- x 12) (+ y 12))
              (love.graphics.print (tostring (- i 1)) (- x 12) (+ y 12))
          )
          )
        (love.graphics.setColor 1 1 1)
        (love.graphics.line x (+ y 4) x (+ y 8) )
        
        )
      )
    (love.graphics.setColor 0.5 0.9 0.5)
    (love.graphics.rectangle "fill" (+ x-min (* drawn-player tile-width)) (- y 8) tile-width tile-width)
    (if (= 1 (. level (+ drawn-player sum)))
        (love.graphics.setColor 1 0 0)
        (love.graphics.setColor 0 1 0)
        )
    (love.graphics.rectangle "line" (+ x-min (* (+ drawn-player sum) tile-width)) (- y 8) tile-width tile-width)
    )
  )

(fn love.draw []
  (when (~= 2 game-state)
    (love.graphics.setColor 0 0.2 0.4)
    (love.graphics.rectangle "fill" 0 200 1080 350)
    (draw-level)
    (draw-dices)
    (draw-special)
    (love.graphics.setColor 1 1 1)
    (love.graphics.print (.. "Moves : " (tostring moves)) 10 500 0 1.5 1.5)
    (love.graphics.print (.. "Rolls left : " (tostring roll-counter)) 10 525 0 1.5 1.5)
    (love.graphics.printf helper-text -300 690 1080 "center" 0 1.5 1.5)
    )
  (when (= 2 game-state)
    (love.graphics.print "VICTORY" 400 150 0 5 5)
    (love.graphics.print (.. "Moves : " (tostring moves)) 440 400 0 3 3)
    (love.graphics.print (.. "Score : " (tostring (math.floor (/ 2800 moves)))) 410 475 0 3 3)
    )
  )
