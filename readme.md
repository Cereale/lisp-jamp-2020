Made for Automn Lisp Jam 2020 : https://itch.io/jam/autumn-lisp-game-jam-2020

Itch page : https://cereale.itch.io/yahtzee-adventure

# Requirements

- Fennel 0.6.0 (https://fennel-lang.org/)
- Lua 5.3.3 (https://www.lua.org/)
- LÖVE 11.3 (https://love2d.org/)

# Run
```sh 
love . 
```

# Controls

- Roll dices : Space
- Confirm roll : Return
- Lock dice : 1-6
- Use combo : Q W E R T
- Select dices (when ability is used) : arrows


# Rules 

- Reach the other end of the level using dice rolls.
- You have 3 rolls per round.
- In between rolls you can lock and unlock certain dices to keep them to the next roll
- If you have a certain combo and once per round, you can use the special ability associated (see below)
- You can't do combos using zeros

# Abilities

- 2 of a kind : select one dice and reroll it
- 3 of a kind : remove one dice
- Full (3 of a kind + 2 of a kind) : add one dice
- 4 of a kind : +1 roll for this round
- Yahtzee (5 of a kind) : select a dice and pick its value

