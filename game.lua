__fnl_global__window_2dwidth = 1080
__fnl_global__window_2dheight = 720
__fnl_global__level_2dlength = 1000
__fnl_global__max_2ddice_2dvalue = 6
__fnl_global__level_2dset = {{2, 2, 2, 2, 2, 2, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 1, 1}, {2, 2, 1, 2, 1, 2, 1, 2, 2, 2, 2, 2, 1, 2, 1, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1}, {2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2}, {2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2}, {2, 2, 2, 1, 1, 1, 2, 2, 2, 1, 1, 1, 2, 2, 2, 1, 1, 1, 2, 2, 2, 1, 1, 1, 2}, {2, 2, 1, 2, 2, 1, 2, 2, 1, 2, 2, 1, 2, 2, 1, 2, 2, 1, 2, 2, 1, 2, 2, 1, 2}, {2, 2, 1, 1, 2, 2, 1, 1, 2, 2, 1, 1, 2, 2, 1, 1, 2, 2, 1, 1, 2, 2, 1, 1, 2}, {2, 1, 2, 1, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2}, {2, 2, 1, 2, 2, 1, 2, 2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2}, {2, 1, 2, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2}, {2, 1, 2, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1, 2, 2, 2}}
local function add(x, y)
  return (x + y)
end
local function dice(max_value)
  return love.math.random(0, max_value)
end
local function max(table)
  local max_value = -1.0000000000000001e+25
  for i = 1, #table do
    if (max_value < table[i]) then
      max_value = table[i]
    end
  end
  return max_value
end
local function min(table)
  local min_value = 1.0000000000000001e+25
  for i = 1, #table do
    if (min_value > table[i]) then
      min_value = table[i]
    end
  end
  return min_value
end
local function mod1(value, step)
  return (1 + ((value - 1) % step))
end
local function reduce(tbl, fct)
  local counter = tbl[1]
  for i = 2, #tbl do
    counter = fct(counter, tbl[i])
  end
  return counter
end
local function count_duplicates(dices, value, dices_active)
  local counter = 0
  for i = 1, #dices do
    if (dices[i] == value) then
      if (dices_active[i] == 1) then
        counter = (counter + 1)
      end
    end
  end
  return counter
end
local function get_combo(dices, dices_active)
  local combo = {0, 0, 0, 0, 0}
  local duplicates = {}
  for i = 1, __fnl_global__max_2ddice_2dvalue do
    duplicates[i] = count_duplicates(dices, i, dices_active)
  end
  table.sort(duplicates)
  do
    local max0 = duplicates[#duplicates]
    local second_max = duplicates[(#duplicates - 1)]
    if (max0 == 5) then
      combo = {1, 1, 0, 1, 1}
    elseif (max0 == 4) then
      combo = {1, 1, 0, 1, 0}
    elseif (max0 == 2) then
      combo = {1, 0, 0, 0, 0}
    end
    if (max0 == 3) then
      if (second_max == 2) then
        combo = {1, 1, 1, 0, 0}
      else
        combo = {1, 1, 0, 0, 0}
      end
    end
  end
  return combo
end
local function reset_round()
  __fnl_global__game_2dstate = 1
  dices = {0, 0, 0, 0, 0, 0}
  __fnl_global__locked_2ddices = {0, 0, 0, 0, 0, 0}
  __fnl_global__roll_2dcounter = 3
  __fnl_global__dices_2dactive = {1, 1, 1, 1, 1, 0}
  combo = get_combo(dices, __fnl_global__dices_2dactive)
  __fnl_global__combo_2dused = false
  selector = 0
  return nil
end
local function build_level()
  level = {}
  local len = #__fnl_global__level_2dset[1]
  for i = 1, __fnl_global__level_2dlength, len do
    local level_brick = __fnl_global__level_2dset[love.math.random(1, #__fnl_global__level_2dset)]
    for j = i, (i + len) do
      level[j] = level_brick[mod1(j, len)]
    end
  end
  return nil
end
love.load = function()
  love.window.setMode(__fnl_global__window_2dwidth, __fnl_global__window_2dheight)
  love.graphics.setBackgroundColor(0, 0.074999999999999997, 0.14999999999999999)
  love.keyboard.setKeyRepeat(true)
  player = 1
  build_level()
  reset_round()
  __fnl_global__combo_2dused = true
  __fnl_global__camera_2doffset = 0
  sum = 0
  checkpoint = 1
  moves = 0
  __fnl_global__helper_2dtext = ""
  return nil
end
local function roll_dice(index)
  dices[index] = dice(6)
  return nil
end
local function handle_combos()
  if (__fnl_global__game_2dstate == 11) then
    selector = 1
    __fnl_global__game_2dstate = 21
  end
  if (__fnl_global__game_2dstate == 12) then
    selector = 1
    __fnl_global__game_2dstate = 22
  end
  if (__fnl_global__game_2dstate == 13) then
    dices[6] = dice(6)
    __fnl_global__dices_2dactive[6] = 1
    __fnl_global__game_2dstate = 1
  end
  if (__fnl_global__game_2dstate == 14) then
    __fnl_global__roll_2dcounter = (__fnl_global__roll_2dcounter + 1)
    __fnl_global__game_2dstate = 1
  end
  if (__fnl_global__game_2dstate == 15) then
    selector = 1
    __fnl_global__game_2dstate = 25
    __fnl_global__dices_2dcopy = {}
    for i = 1, 6 do
      __fnl_global__dices_2dcopy[i] = dices[i]
    end
  end
  __fnl_global__combo_2dused = true
  return nil
end
local function dice_sum()
  local list = {}
  for i = 1, #dices do
    if (1 == __fnl_global__dices_2dactive[i]) then
      if (dices[i] ~= nil) then
        list[i] = dices[i]
      end
    end
  end
  for i = #list, 1, -1 do
    if (nil == list[i]) then
      table.remove(list, i)
    end
  end
  local out = reduce(list, add)
  return out
end
local function set_helper_text()
  if (__fnl_global__game_2dstate == 1) then
    if (__fnl_global__roll_2dcounter > 0) then
      __fnl_global__helper_2dtext = "Roll the dices (Space), use an ability (QWERT) or confirm roll (Enter)"
      return nil
    else
      __fnl_global__helper_2dtext = "Use an ability (QWERT) or confirm roll (Enter)"
      return nil
    end
  elseif (__fnl_global__game_2dstate == 21) then
    __fnl_global__helper_2dtext = "2 of a kind : Select (arrows) dice to reroll and confirm (Enter)"
    return nil
  elseif (__fnl_global__game_2dstate == 22) then
    __fnl_global__helper_2dtext = "3 of a kind : Select (arrows) dice to remove and confirm (Enter)"
    return nil
  elseif (__fnl_global__game_2dstate == 25) then
    __fnl_global__helper_2dtext = "Yahtzee : Select (arrows) dice and change its value (arrows)"
    return nil
  end
end
love.update = function(dt)
  if (__fnl_global__game_2dstate == 1) then
    if (level[player] == 1) then
      player = checkpoint
    else
      __fnl_global__camera_2doffset = max({0, (player - 10)})
    end
    if (player > __fnl_global__level_2dlength) then
      __fnl_global__game_2dstate = 2
      player = (2 + __fnl_global__level_2dlength)
      print("Victory")
    end
  end
  if (__fnl_global__game_2dstate > 10) then
    handle_combos()
  end
  sum = dice_sum()
  checkpoint = (1 + (100 * math.floor((player / 100))))
  return set_helper_text()
end
love.keypressed = function(key, scancode, isrepeat)
  if (key == "space") then
    if (__fnl_global__game_2dstate == 1) then
      if (__fnl_global__roll_2dcounter > 0) then
        for i = 1, 6 do
          if (__fnl_global__locked_2ddices[i] == 0) then
            if (__fnl_global__dices_2dactive[i] == 1) then
              dices[i] = dice(__fnl_global__max_2ddice_2dvalue)
            end
          end
        end
        __fnl_global__roll_2dcounter = (__fnl_global__roll_2dcounter - 1)
        combo = get_combo(dices, __fnl_global__dices_2dactive)
      end
    end
  end
  if (key == "return") then
    if (__fnl_global__game_2dstate == 1) then
      player = (player + dice_sum())
      reset_round()
      __fnl_global__camera_2doffset = max({0, (player - 30)})
      moves = (moves + 1)
    end
    if (__fnl_global__game_2dstate == 21) then
      roll_dice(selector)
      __fnl_global__game_2dstate = 1
      selector = 0
    end
    if (__fnl_global__game_2dstate == 22) then
      __fnl_global__dices_2dactive[selector] = 0
      __fnl_global__locked_2ddices[selector] = 0
      __fnl_global__game_2dstate = 1
      selector = 0
    end
    if (__fnl_global__game_2dstate == 25) then
      do
        local temp = dices[selector]
        for i = 1, 6 do
          dices[i] = __fnl_global__dices_2dcopy[i]
        end
        dices[selector] = temp
      end
      __fnl_global__game_2dstate = 1
      selector = 0
    end
  end
  if (key == "1") then
    __fnl_global__locked_2ddices[1] = (1 - __fnl_global__locked_2ddices[1])
  elseif (key == "2") then
    __fnl_global__locked_2ddices[2] = (1 - __fnl_global__locked_2ddices[2])
  elseif (key == "3") then
    __fnl_global__locked_2ddices[3] = (1 - __fnl_global__locked_2ddices[3])
  elseif (key == "4") then
    __fnl_global__locked_2ddices[4] = (1 - __fnl_global__locked_2ddices[4])
  elseif (key == "5") then
    __fnl_global__locked_2ddices[5] = (1 - __fnl_global__locked_2ddices[5])
  elseif (key == "6") then
    __fnl_global__locked_2ddices[6] = (1 - __fnl_global__locked_2ddices[6])
  end
  if (selector == 0) then
    if (key == "right") then
      __fnl_global__camera_2doffset = (__fnl_global__camera_2doffset + 1)
    elseif (key == "left") then
      __fnl_global__camera_2doffset = (__fnl_global__camera_2doffset - 1)
    elseif (key == "down") then
      __fnl_global__camera_2doffset = max({0, (player - 30)})
    end
  end
  if (selector > 0) then
    if (__fnl_global__game_2dstate == 25) then
      if (key == "up") then
        dices[selector] = mod1((dices[selector] + 1), 6)
      end
      if (key == "down") then
        dices[selector] = mod1((dices[selector] - 1), 6)
      end
    end
    if (key == "right") then
      if (__fnl_global__game_2dstate == 25) then
        dices[selector] = __fnl_global__dices_2dcopy[selector]
      end
      selector = mod1((selector + 1), 6)
      while (0 == __fnl_global__dices_2dactive[selector]) do
        selector = mod1((selector + 1), 6)
      end
    end
    if (key == "left") then
      if (__fnl_global__game_2dstate == 25) then
        dices[selector] = __fnl_global__dices_2dcopy[selector]
      end
      selector = mod1((selector - 1), 6)
      while (function(_7_,_8_,_9_) return (_7_ == _8_) and (_8_ == _9_) end)(0,__fnl_global__dices_2dactive[selector],6) do
        selector = mod1((selector - 1), 6)
      end
    end
  end
  if (__fnl_global__combo_2dused == false) then
    if (key == "q") then
      __fnl_global__game_2dstate = 11
      return nil
    elseif (key == "w") then
      __fnl_global__game_2dstate = 12
      return nil
    elseif (key == "e") then
      __fnl_global__game_2dstate = 13
      return nil
    elseif (key == "r") then
      __fnl_global__game_2dstate = 14
      return nil
    elseif (key == "t") then
      __fnl_global__game_2dstate = 15
      return nil
    end
  end
end
local function draw_dices()
  local x_min = 200
  local y = 600
  local w = 50
  for i = 1, 5 do
    local x = (x_min + (2 * w * i))
    if (__fnl_global__dices_2dactive[i] == 1) then
      if (__fnl_global__locked_2ddices[i] == 0) then
        love.graphics.setColor(1, 1, 1)
      else
        love.graphics.setColor(0.69999999999999996, 0.5, 0.5)
      end
      love.graphics.rectangle("line", x, y, w, w)
      love.graphics.print(tostring(dices[i]), x, y, 0, 2, 2, (w / -6), (w / -9))
      love.graphics.print(tostring(i), x, y, 0, 1, 1, -2, -1)
    end
    if (selector == i) then
      love.graphics.setColor(1, 1, 0.69999999999999996)
      love.graphics.rectangle("line", (x - 2), (y - 2), (w + 4), (w + 4))
    end
  end
  if (__fnl_global__dices_2dactive[6] == 1) then
    if (__fnl_global__locked_2ddices[6] == 0) then
      love.graphics.setColor(1, 1, 1)
    else
      love.graphics.setColor(0.69999999999999996, 0.5, 0.5)
    end
    love.graphics.rectangle("line", (x_min + (2 * w * 6)), y, w, w)
    love.graphics.print(tostring(dices[6]), (x_min + (2 * w * 6)), y, 0, 2, 2, (w / -6), (w / -6))
  end
  love.graphics.setColor(1, 1, 1)
  return love.graphics.print(tostring(sum), 535, 215, 0, 2, 2)
end
local function draw_special()
  do
    local x_min = -100
    local y = 75
    local h = 50
    local w = 130
    local combo_names = {"2 of a kind", "3 of a kind", "Full", "4 of a kind", "Yahtzee"}
    local keys = {"Q", "W", "E", "R", "T"}
    for i = 1, 5 do
      if (combo[i] == 0) then
        love.graphics.setColor(1, 1, 1)
      else
        if (__fnl_global__combo_2dused == false) then
          love.graphics.setColor(0.5, 0.90000000000000002, 0.5)
        end
      end
      local x = (x_min + (1.5 * w * i))
      love.graphics.rectangle("line", x, y, w, h)
      love.graphics.print(combo_names[i], x, y, 0, 1.5, 1.5, (w / -10), (h / -6))
      love.graphics.print(tostring(keys[i]), x, y, 0, 0.90000000000000002, 0.90000000000000002, -2, -1)
    end
  end
  return love.graphics.setColor(1, 1, 1)
end
local function draw_level()
  local y = 400
  local tile_width = 16
  local x_min = (100 - (__fnl_global__camera_2doffset * tile_width))
  local drawn_player = player
  love.graphics.setColor(1, 1, 1)
  love.graphics.line((x_min + tile_width), (y + (tile_width / 2)), (x_min + (__fnl_global__level_2dlength * tile_width) + tile_width), (y + (tile_width / 2)))
  for i = 1, (1 + __fnl_global__level_2dlength) do
    local x = (x_min + (i * tile_width))
    if (level[i] == 1) then
      love.graphics.setColor(0.5, 0.40000000000000002, 0.80000000000000004)
      love.graphics.rectangle("fill", x, (y - 8), tile_width, tile_width)
    end
    if (1 == mod1(i, 100)) then
      if (checkpoint >= i) then
        love.graphics.setColor(0.5, 0.90000000000000002, 0.5)
      else
        love.graphics.setColor(0.90000000000000002, 0.90000000000000002, 0.5)
      end
      love.graphics.line(x, (y + 4), x, (y - 32))
      love.graphics.polygon("fill", x, (y - 24), x, (y - 32), (x + 8), (y - 28))
      if (i == 1) then
        love.graphics.print("Start", (x - 12), (y + 12))
      elseif (i == 1001) then
        love.graphics.print("Finish", (x - 12), (y + 12))
      else
        love.graphics.print(tostring((i - 1)), (x - 12), (y + 12))
      end
    end
    love.graphics.setColor(1, 1, 1)
    love.graphics.line(x, (y + 4), x, (y + 8))
  end
  love.graphics.setColor(0.5, 0.90000000000000002, 0.5)
  love.graphics.rectangle("fill", (x_min + (drawn_player * tile_width)), (y - 8), tile_width, tile_width)
  if (1 == level[(drawn_player + sum)]) then
    love.graphics.setColor(1, 0, 0)
  else
    love.graphics.setColor(0, 1, 0)
  end
  return love.graphics.rectangle("line", (x_min + ((drawn_player + sum) * tile_width)), (y - 8), tile_width, tile_width)
end
love.draw = function()
  if (2 ~= __fnl_global__game_2dstate) then
    love.graphics.setColor(0, 0.20000000000000001, 0.40000000000000002)
    love.graphics.rectangle("fill", 0, 200, 1080, 350)
    draw_level()
    draw_dices()
    draw_special()
    love.graphics.setColor(1, 1, 1)
    love.graphics.print(("Moves : " .. tostring(moves)), 10, 500, 0, 1.5, 1.5)
    love.graphics.print(("Rolls left : " .. tostring(__fnl_global__roll_2dcounter)), 10, 525, 0, 1.5, 1.5)
    love.graphics.printf(__fnl_global__helper_2dtext, -300, 690, 1080, "center", 0, 1.5, 1.5)
  end
  if (2 == __fnl_global__game_2dstate) then
    love.graphics.print("VICTORY", 400, 150, 0, 5, 5)
    love.graphics.print(("Moves : " .. tostring(moves)), 440, 400, 0, 3, 3)
    return love.graphics.print(("Score : " .. tostring(math.floor((2800 / moves)))), 410, 475, 0, 3, 3)
  end
end
return love.draw
